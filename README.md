

***
*Source code for the article by Denton et al., 2020 has been moved [here](https://gitlab.com/simm/gc/hyphasma/-/releases/Denton2020)*
***
***

This  repository contains the source code used to generate the simulations described in
the article "Injection of antibodies against immunodominant epitopes tunes germinal 
centres to generate broadly neutralizing antibodies" by Michael Meyer-Hermann. The 
branches correspond to the different source code versions used for the figures in the 
article; the "master" branch corresponds to the reference version, the others are slight 
variations thereof.

In order to generate the figures, the code has to be run multiple times and results must
be stored in their own directory. The scripts for generating the figures expect the
results in numbered directories below ~/Work/hyphasma/hyphasma/results/.

Parameter files and source code used for the figures:

# Figure 1
- no ab feedback with two epitopes in different ratios
- branch master
***
- (A) multiag041_2agH8frac50
- (B) multiag041_2agH8frac30
- (C) multiag041_2agH8frac10
- (D) multiag041_2agH8frac03

# Figure 2
- with ab feedback and injected ab (different conc)
- branch master
***
- (A) multiag042ab2agH8frac10
- (B) multiag042ab2agH8frac10inject5e-10ss6666t72
- (C) multiag042ab2agH8frac10inject5e-09ss6666t72
- (D) multiag042ab2agH8frac10inject5e-08ss6666t72

# Figure 3
- same with 120 randomly started simulations
- branch master
***
- multiag042ab2agH8frac10x
- multiag042ab2agH8frac10inject5e-10ss6666t72x
- multiag042ab2agH8frac10inject5e-09ss6666t72x
- multiag042ab2agH8frac10inject5e-08ss6666t72x


# Figure 4
- memory-derived plasma cells producing antibody
- branch master
***
- multiag044ab2agH8frac50PC10000mem05x
- multiag044ab2agH8frac50PC1000mem05x
- multiag044ab2agH8frac50PC0mem05x

# Table 1
- branch master
***
- col. 1: multiag042ab2agH8frac03x                                 Table 1, col 1
- col. 2: multiag042ab2agH8frac03inject5e-09ss6666t72x             Table 1, col 2
- col. 3: multiag042ab2agH8frac03inject5e-08ss6666t72x             Table 1, col 3
- col. 4: multiag044ab2agH8frac03PC1000mem90x                      Table 1, col 4
- col. 5: multiag044ab2agH8frac03PC1000mem25x                      Table 1, col 5
- col. 6: multiag044ab2agH8frac03PC1000mem05x                      Table 1, col 6
- col. 6: multiag044ab2agH8frac03PC10000mem05x                     Table 1, col 7


# Figure S1
- no ab feedback, single epitope
- branch master
***
- multiag040

note that panels A,B,C were generated with gle

# Figure S2
- with ab feedback, single epitope
- branch master
***
- multiag040ab

note that panels A,B,C were generated with gle

# Figure S3
- Ag uptake model: highest affinity vs amount
- branch master
***
- A, B, C multiag042ab2agH8frac10inject5e-09ss6666t72
- D, E, F multiag042ab2agH8frac10inject2_5e-09ss6666t72_APCtakehighamount

# FigureS4
- GC exit model: LEDA vs LELI
- branch master
***
- A, B, C: multiag045ab2agH8frac10inject5e-09ss6666t72
- D, E, F: multiag046ab2agH8frac10inject5e-09ss6666t72

# Figure S5
- DSH switched off
- branch master
***
- multiag042ab2agH8frac10noDSHx
- multiag042ab2agH8frac10inject5e-10ss6666t72noDSHx
- multiag042ab2agH8frac10inject5e-09ss6666t72noDSHx
- multiag042ab2agH8frac10inject5e-08ss6666t72noDSHx

# Figure S6
- affinity-independent epitope uptake
- branch master
***
- multiag042ab2agH8frac10noaffx
- multiag042ab2agH8frac10inject5e-08ss6666t72noaffx

# Figure S7
- no feedback of antibodies on epitope presentation      
- branch noAbfeedback
***
- multiag042ab2agH8frac10nofeedbackx
- multiag042ab2agH8frac10inject5e-08ss6666t72nofeedbackx


# Figure S8
- no pMHC-dependent Tfh polarisation
- branch noTfhpol
***
- multiag042ab2agH8frac10noTfhpolx
- multiag042ab2agH8frac10inject5e-10ss6666t72noTfhpolx
- multiag042ab2agH8frac10inject5e-09ss6666t72noTfhpolx
- multiag042ab2agH8frac10inject5e-08ss6666t72noTfhpolx

# Figure S9
- no DND
- branch master
***
- multiag042ab2agH8frac10noDNDx
- multiag042ab2agH8frac10inject5e-10ss6666t72noDNDx
- multiag042ab2agH8frac10inject5e-09ss6666t72noDNDx
- multiag042ab2agH8frac10inject5e-08ss6666t72noDNDx

# Figure S10
- no chemokine sensitivity
- branch master
***
- multiag042ab2agH8frac10nochemotaxisx
- multiag042ab2agH8frac10inject5e-10ss6666t72nochemotaxisx
- multiag042ab2agH8frac10inject5e-09ss6666t72nochemotaxisx
- multiag042ab2agH8frac10inject5e-08ss6666t72nochemotaxisx

# Figure S11
- LEDAX model
- branch master*
***
- multiag045ab2agH8frac10x
- multiag045ab2agH8frac10inject5e-10ss6666t72x
- multiag045ab2agH8frac10inject5e-09ss6666t72x
- multiag045ab2agH8frac10inject5e-08ss6666t72x

# Figure S12
- LELI model
- branch master
***
- multiag046ab2agH8frac10x
- multiag046ab2agH8frac10inject5e-10ss6666t72x
- multiag046ab2agH8frac10inject5e-09ss6666t72x
- multiag046ab2agH8frac10inject5e-08ss6666t72x

#Figure S13 A
- LELI model with varied recycling
- branch master
***
- multiag046ab2agH8frac10r80x
- multiag046ab2agH8frac10r80inject5e-10ss6666t72x          
- multiag046ab2agH8frac10r80inject5e-09ss6666t72x          
- multiag046ab2agH8frac10r80inject5e-08ss6666t72x

# Figure S13 B
- LELI model with varied recycling
- branch master
***
- multiag046ab2agH8frac10r95x                              
- multiag046ab2agH8frac10r95inject5e-10ss6666t72x          
- multiag046ab2agH8frac10r95inject5e-09ss6666t72x          
- multiag046ab2agH8frac10r95inject5e-08ss6666t72x          
