#################################################################################################
# Generates a box plot for multiple copies of simulations based on different models.
# In this case it was used to evaluate the accumulated antibody production for a specific epitope
# as saved in antibody_ag4444.out, which was meant as the low accessible epitope.
# Only the highest affinity bin is included.
# MMH 2018-10-05
#################################################################################################
library(ggplot2)
suppressMessages(library(Hmisc))
options(warn=-1)
EvaluateAbAtTime = 504
#################################################################################################
## @brief Extract data for FracHA_Tselect_selspeed.txt for a single GC 
## @param gcpath string path to data files for germinal center
## @return data.frame 
get_singlesim_data <- function(gcpath,file,cnames,cols) {
  cdf <- read.table(paste(gcpath,"/",file, sep=""), header=FALSE, skip=2)
  cdf <- cdf[,cols]
  names(cdf) <- cnames
  cdf <- cdf[cdf$time == EvaluateAbAtTime,]
  cdf
}
#################################################################################################
## @brief Read data for all GCs in one in silico experiment
## @param folder string folder with one subfolder for each GC
## @return data.frame 
read_data <- function(folder,file,cnames,cols) {
  count=0
  GCPaths <- list.dirs(path = folder, full.names = TRUE, recursive = FALSE)
  for (ifolder in GCPaths) {
     count=count+1
     if (count==1) { 
	AllDataSets <- get_singlesim_data(ifolder,file,cnames,cols)
     } else {
	next_data <- get_singlesim_data(ifolder,file,cnames,cols)
	AllDataSets <- rbind(AllDataSets,next_data)
     }
  }
  AllDataSets
}
#################################################################################################
getmean <- function(dt) {
  times <- unique(dt$time)
  max_time <- max(times)
  dtcollect <- NULL
  dtcollectnamest <- NULL
  for (i in 1:max_time) {
    dtday <- dt[ dt$time==i, ]
    dtdaymeansd <- c(i)
    dtcollectnames <- c("time")
    dtcollectnamest <- c(dtcollectnamest, paste("day",i,sep=""))
    for (cols in 2:length(names(dt))) { 
      dtdaymeansd <- c(dtdaymeansd, mean(dtday[,cols]), sd(dtday[,cols]))
      dtcollectnames <- c(dtcollectnames, names(dt)[cols], paste(names(dt)[cols],"_SD",sep=""))
    }
    dtcollect <- rbind(dtcollect, dtdaymeansd)
  }
  dimnames(dtcollect) <- list(dtcollectnamest,dtcollectnames)
  dtcollect
}
#################################################################################################
mktable <- function(simname, cname) {
  #cols <- c(1,13,14)
  cols <- c(1,11,13)
  path=paste("~/Work/hyphasma/hyphasma/results/",simname,"/",sep="")
  infile=paste("antibody_ag4444.out", sep="")
  namesc <- c("time",cname,"injected")
  dtf <- read_data(path,infile,namesc,cols)
  # This contains a list of lines for time 504 with the entry of bin 10 and injected ab
  # for 4444 the injected Ab is irrelevant because it doesn't contribute to bin 10
  # for 6666 the injected Ab has to be deduced from bin 10 in order to get the endogenous part
  dtf
}
#################################################################################################
read_all_sims <- function() {
  path=paste("multiag042ab2agH8frac10noaffx-all")
  cname <- "0"
  collected0 <- mktable(path, cname)
  #
  path=paste("multiag042ab2agH8frac10inject5e-08ss6666t72noaffx-all")
  cname <- "50"
  collected50 <- mktable(path, cname)
  #
  mergeddata <- c((collected0[2]+collected0[3])*1.e+09,
                  (collected50[2]+collected50[3])*1.e+09)
  #cors2 <- cor.test(mergeddata[2], mergeddata[3], method="pearson")
  #print(cors2$estimate,cors2$p.value,cors2$conf.int)
  mergeddata
}
#################################################################################################
library(ggpubr)
library(magrittr)
library(reshape2)
mkboxplot <- function(dat,file) {
  # Creates boxplots for each element of the list `dat` and adds p-values for
  # comparisons between the sets specified below (as identified by the `names`
  # attribute of `dat`.

  # Specify which sets to compare
  setstocompare <- list(c("0", "50"))
  plt <- dat %>% as.data.frame %>% set_names(names(dat)) %>% melt %>% 
    ggboxplot(x="variable", y="value") + 
    stat_compare_means(comparisons=setstocompare, method="wilcox.test",
                       p.adjust.method="BH") +
    theme(text=element_text(size=20)) +
    xlab("Injected antibody specific for immunodominant epitope [nMol]") +
    ylab("Generated antibody specific for low accessible epitope [nMol]")
  plt_tmp <- ggplot_build(plt)
  #print(plt_tmp$data)
  ggsave(paste0(file, ".eps"),width=10, height=10)
}
#################################################################################################


mergeddata <- read_all_sims()
mkboxplot(mergeddata, "FigureS6")
#################################################################################################

